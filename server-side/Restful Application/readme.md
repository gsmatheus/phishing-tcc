# Visão geral

Representational State Transfer, geralmente chamado pelo acrônimo REST, trata-se de Arquitetura web responsável por servir interfaces de programação e oferecer um conjunto de rotinas e padrões para fazer o controle das ações baseadas nos dados gerados/capturados no lado do cliente/usuário usando integralmente mensagens HTTP para se comunicarem entre si.

O Usuário faz a requisição para uma rota especifica, empacotando o cabeçalho e os dados por meios de métodos de requisição(GET,POST,PUT...) e então o servidor recebe pelo protocolo HTTP e trata os dados recebidos. Em seguida, o servidor devolve uma resposta ao usuário.

## Instalação

Primeiramente instale o [NodeJS v12.16.0](https://nodejs.org/en/) em sua maquina ou com uma versão maior ou igual a 6.12.0 do [NPM](https://nodejs.org/en/)

Em seguida, será necessário instalar os pacotes que são utilizados para constituir esta interface de aplicação(API). Para isto, basta abrir o terminal e acessar este diretório em sua maquina. Com o terminal aberto e o diretório apontado, basta utilizar o comando abaixo para iniciar a instalação dos pacotes:


```bash
npm install
```
Feito a instalação dos pacotes, será necessário abrir a porta 3100 em seu firewall. 

E por fim, para iniciar o servidor da API Rest, utilizamos o comando abaixo:
```bash
npm start
```
Uma importante observação, é ficar atento a prefixos "ERR" com uma cor vermelha, pois isto significa que há problemas de versões ou pode significar que houve problemas na instalação.

## Modo de uso

Para testar os serviços de API Rest, é recomendável que tenha o [Postman](https://www.getpostman.com/) instalado em sua maquina! Pois ele oferece vários recursos uteis para realizar requisições.

No caso desta API, é possível realizar requisições de método GET e POST. Com métodos GET é possível transmitir informações somente pela URL da rota. Método POST tem como regra e padrão, a passagem de parâmetros por meios de formulários.
No caso desta API, existe somente uma rota que utiliza o metodo GET e não há necessidade de enviar parametros para a mesma.

## Rotas

Para solicitar uma hash ao servidor, podemos acessar a rota abaixo utilizando o método GET
<ul>
    <li>GET</li>
        <ul>
            <li>http://127.0.0.1:3100/v1/user</li>
        </ul>
</ul>

Pode-se utilizar a rota abaixo para gerar um LOG referente a pagina em que o usuário acessou informando o <b>ID da Pagina</b> e o <b>Hash do usuário</b>
<ul>
    <li>POST</li>
        <ul>
            <li>http://127.0.0.1:3100/v1/logaccess</li>
            <li><b>Parâmetros</b>: [hash,page]</li>
        </ul>
</ul>

Nesta API, é possivel capturar informações de localidade do usuário a partir do endereço de IP. Para usufruir deste, basta requisitar a rota abaixo enviando o <b>IP</b> do usuário
<ul>
    <li>POST</li>
        <ul>
            <li>http://127.0.0.1:3100/v1/ipaddress</li>
            <li><b>Parâmetro</b>: [ip]</li>
        </ul>
</ul>