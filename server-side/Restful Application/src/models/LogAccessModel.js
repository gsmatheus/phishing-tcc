import db from '../config/database';

export function AddLogAccess(page,hash) {
    const query = `INSERT INTO LogAccess (idAccessPage,hash_session) values (${page},'${hash}')`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}