import db from '../config/database';
const request = require('request');

export function InsertIPAddress(ip,address){
    let query = `INSERT INTO IPAddress( IPAddress,idPhysicalAddress) values ('${ip}',${address})`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}

export function GetInformationsByIP(ip){
    let url = `https://api.ipgeolocation.io/ipgeo?apiKey=34cf0868d4224b9d86875bd2389641ee&ip=${ip}`

    return new Promise( (resolve, reject) => {
        request.get(url, (err, response) => {
            resolve(response)
        }, err => {
            reject(err)
        })
    });
}


