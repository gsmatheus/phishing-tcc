import db from '../config/database';

export function InsertCountryAddress(acronym_country){
    let query = `INSERT INTO CountryAddress( acronym_country) values ('${acronym_country}')`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err){
                if (err.sqlState == 23000){
                    SelectCountryAddress(acronym_country).then((response) => {
                       resolve(response[0].idCountryAddress)
                    })
                } else {
                    reject(err)
                }
             } else {
                resolve(result.insertId);
             }
        });
    });
}

export function InsertCityAddress(city_name){
    let query = `INSERT INTO CityAddress( city_name) values ('${city_name}')`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err){
                if (err.sqlState == 23000){
                    SelectCityAddress(city_name).then((response) => {
                       resolve(response[0].idCityAddress)
                    })
                } else {
                    reject(err)
                }
             } else {
                resolve(result.insertId);
             }
        });
    });
}

export function InsertStateAddress(acronym_state){
    let query = `INSERT INTO StateAddress( acronym_state) values ('${acronym_state}')`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err){
                if (err.sqlState == 23000){
                    SelectStateAddress(acronym_state).then((response) => {
                       resolve(response[0].idStateAddress)
                    })
                } else {
                    reject(err)
                }
             } else {
                resolve(result.insertId);
             }
        });
    });
}

export function InsertPhysicalAddress(country,city,state){
    let query = `INSERT INTO PhysicalAddress(idCountryAddress,idCityAddress,idStateAddress) values (${country},${city},${state})`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err){
                if (err.sqlState == 23000){
                    SelectPhysicalAddress(country,city,state).then((response) => {
                       resolve(response[0].idPhysicalAddress)
                    })
                } else {
                    reject(err)
                }
             } else {
                resolve(result.insertId);
             }
        });
    });
}

export function SelectCountryAddress(acronym_state){
    let query = `SELECT idCountryAddress FROM CountryAddress WHERE acronym_country ='${acronym_state}'`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}

export function SelectCityAddress(city){
    let query = `SELECT idCityAddress FROM CityAddress WHERE city_name ='${city}'`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}

export function SelectStateAddress(state){
    let query = `SELECT idStateAddress FROM StateAddress WHERE acronym_state ='${state}'`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}

export function SelectPhysicalAddress(country,city,state){
    let query = `SELECT idPhysicalAddress FROM PhysicalAddress WHERE idCountryAddress = ${country} AND idCityAddress = ${city} AND idStateAddress = ${state}`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}