import { Router } from 'express';
import { NewHashUser} from '../controllers/UserController';

const Route = Router();

Route.get('/', NewHashUser);
export default Route;
