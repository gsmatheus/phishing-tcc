import { Router } from 'express';
import { NewIPAddress} from '../controllers/IPController';

const Route = Router();

Route.post('/', NewIPAddress);
export default Route;
