import { Router } from 'express';
import { NewLogAccess} from '../controllers/LogAccessController';

const Route = Router();

Route.post('/', NewLogAccess);
export default Route;
