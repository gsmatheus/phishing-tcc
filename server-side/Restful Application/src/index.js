import express from 'express';
import bodyParser from 'body-parser';

import User from './routes/User';
import LogAccess from './routes/LogAccess';
import IP from './routes/IP';
import CORS from 'cors';

const app = express();
const morgan = require('morgan');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(CORS());

app.use('/v1/user', User);
app.use('/v1/logaccess', LogAccess);
app.use('/v1/ipaddress', IP);

app.listen(3100);