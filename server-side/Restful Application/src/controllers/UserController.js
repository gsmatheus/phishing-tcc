import { AddHashSession } from '../models/UserModel';
import bcrypt from 'bcryptjs';
import {generate} from 'randomstring'

export function NewHashUser(req, res) {
    let randomChars = generate({
        length: 20,
        charset: 'alphabetic'
    })        
    bcrypt.hash(randomChars,10, (err,hash) => {        
        AddHashSession(hash).then((response) => {     
            res.status(200).send({ message: 'Created successfully!', hash: hash });   
        }).catch((error) => {
            console.log(error)
            res.status(500).send({ message: 'There was an error adding information.', error: `${error.code}`, status: false });
        });
    })    
}