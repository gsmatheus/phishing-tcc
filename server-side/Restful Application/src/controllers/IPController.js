import { InsertIPAddress,GetInformationsByIP } from '../models/IPModel';
import { 
    InsertCountryAddress,  
    InsertCityAddress,
    InsertStateAddress,
    InsertPhysicalAddress
} from '../models/PhysicalAddressModel';

export function NewIPAddress(req, res) {
    GetInformationsByIP(req.body.ip).then((response) => {
        let responseJ = JSON.parse(response.body)
        let country = responseJ.country_code2
        let city = responseJ.district
        let state = responseJ.state_prov

        InsertCountryAddress(country).then((countryId) => {
            InsertCityAddress(city).then((cityId) => {
                InsertStateAddress(state).then((stateId) => {
                    InsertPhysicalAddress(countryId,cityId,stateId).then((physicalId) => {
                        InsertIPAddress(req.body.ip,physicalId).then((insertIp) => {
                            res.send(201)
                        })
                    })
                })
            })
        })
      
                        
    })
}