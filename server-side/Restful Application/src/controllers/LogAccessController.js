import { AddLogAccess } from '../models/LogAccessModel';
import bcrypt from 'bcryptjs';


export function NewLogAccess(req, res) {
    AddLogAccess(req.body.page, req.body.hash).then((response) => {
        res.status(200).send({ message: 'Created successfully!', status: true });
    }).catch((error) => {
        console.log(error)
        res.status(500).send({ message: 'There was an error adding information.', error: `${error.code}`, status: false });
    });
}
