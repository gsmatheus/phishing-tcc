import MySQL from 'mysql';

const connection = MySQL.createPool({
    host: '127.0.0.1',
    user: 'root',
    password: '',
    database: 'mydb',
    connectionLimit: 10,
});

export default connection;
